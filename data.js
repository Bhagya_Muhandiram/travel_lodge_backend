const hotels = [
  {
      id: 01,
      name: 'Edinburgh Central Waterloo Place',
      location: 'Edinburgh',
      address:"17 Waterloo Place, Edinburgh,EH1 3BG, United Kingdom ",
      reviews: 4.5,
      from_date:'2020-06-01',
      to_date:'2021-06-01',
      price:29.34,
      description: 'Well placed for a gander at the Athenian acropolis on top of Calton Hill',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0129_EDINBURGH_WATERLOO_PLACE_EXTERIOR.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://images.unsplash.com/photo-1505773278895-5efa7b11679a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://images.unsplash.com/photo-1557729292-f97e48e056ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"}
      ]
    },
    {
      id: 02,
      name: 'Edinburgh Central Princes Street',
      location: 'Edinburgh',
      address:"1Meuse Lane, off Princes Street,Edinburgh, EH2 2BY, United Kingdom ",
      from_date:'2020-06-08',
      to_date:'2021-06-18',
      reviews: 4.0,
      price:31.23,
      description: 'Prime central location and only a short stroll from the train station',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0135_Edinburgh_Central_Princes_St_Exterior_2208x1656.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 03,
      name: 'Edinburgh Central',
      location: 'Edinburgh',
      address:"33 St. Mary's Street, Edinburgh,EH1 1TA, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.7,
      price:26.98,
      description: 'This hotel has had a million pound refurbishment and is just a short walk from Princes Street - perfect for shopaholics.',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0113_EDINBURGH_CENTRAL_EXTERIOR_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 04,
      name: 'Edinburgh Central Rose Street',
      location: 'Edinburgh',
      reviews: 4.4,
      price:30.83,
      address:"37-43 Rose Street, Edinburgh,EH2 2NH, United Kingdom",
      from_date:'2020-06-01',
      to_date:'2021-06-01',
      description: 'Centrally located between New Town and Old Town, and walkable from Edinburgh Castle',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0128_Edinburgh_Central_RoseSt_1024.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
      
    },
    {
      id: 05,
      name: 'Cardiff Central Queen Street',
      location: 'Cardiff',
      address:"The Friary, off Queen Street,CF10 2RG, United Kingdom",
      from_date:'2020-06-08',
      to_date:'2021-06-18',
      reviews: 4.8,
      price:25.47,
      description: 'Perfect for the rugby at Principality Stadium and visiting Cardiff University.',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0425_Cardiff_Central_QueenSt_EXTERIOR.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 06,
      name: 'Cardiff Central',
      location: 'Cardiff',
      address:"Imperial Gate, St. Mary's Street,Cardiff, CF10 1FA, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.7,
      price:31.23,
      description: 'Cardiff attractions are right on your doorstep, with excellent access to transport links"',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0413_Cardiff_Central_EXTERIOR.jpg",
      rrooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 07,
      name: 'Cardiff Atlantic Wharf',
      location: 'Cardiff',
      address:"Atlantic Wharf, Hemingway Road,Cardiff, CF10 4JY, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 3.9,
      price:27.64,
      description: 'Perfectly placed to enjoy Cardiff Bay and still close to the city centre',
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0419_Cardiff_Atlantic_Wharf_EXTERIOR.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 08,
      name: 'Cardiff Whitchurch',
      location: 'Cardiff',
      address:"21 Tyn-y-Parc Road, Whitchurch,Cardiff, CF14 6BH, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.0,
      price:30.56,
      description: "A short drive from Cardiff city centre and just a few minutes from Cardiff Metropolitan University",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB1211_Cardiff_Whitchurch_EXTERIOR.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 09,
      name: 'London Convent Garden',
      location: 'London',
      address:"10 Drury Lane, High Holborn,London, WC2B 5RE, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.8,
      price:28.59,
      description: "Superbly located in the heart of the West End. Ideal for seeing a show or perusing the British Museum.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0783_LONDON_COVENT_GARDEN_EXTERIOR_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 10,
      name: 'London Central Waterloo',
      location: 'London',
      address:"195 - 203 Waterloo Road, SE1 8UX, United Kingdom ,Sat nav postcode: SE1 8UX ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.8,
      price:24.78,
      description: "This hotel has had a million pound refurbishment and you're a 20 minute walk from the London Eye and South Bank.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0914_LONDON_CENTRAL_WATERLOO_EXTERIOR_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 11,
      name: 'London Central SouthWark',
      location: 'London',
      address:"202 - 206 Union Street, Southwark,London, SE1 0LX, United Kingdom",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.7,
      price:26.83,
      description: "Conveniently located just a 3 minute walk from Southwark tube station, great for a visit to The Shard or a trip around Borough Market",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0920_London_Central_Southwark_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 12,
      name: 'London Central Euston',
      location: 'London',
      address:"1-11 Grafton Place, London,NW1 1DJ, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.6,
      price:28.56,
      description: "Travel with ease with Euston station right on your doorstep or explore London Zoo or Madame Tussauds for a fun day out.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0912_LONDON_CENTRAL_EUSTON_EXTERIOR_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 13,
      name: 'Haverhill',
      location: 'London',
      address:"10 - 42 Kings Cross Road, WC1X 9QE, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.3,
      price:28.67,
      description: "Just a 15 minute walk from Kings Cross and lively Farringdon. And now featuring a new Bar Café and SuperRooms.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0789_London_Farringdon_New_Style_Exterior_Ticker_2208x1656_0.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 14,
      name: 'Cambridge Fourwentways',
      location: 'Cambridge',
      address:"A11 Fourwentways, Abington,CB21 6AP, United Kingdom",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.7,
      price:30.45,
      description: "Handy for the conference centre and business park, on the outskirts of Cambridge",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0613_Cambridge_Fourwentways_EXTERIOR.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 15,
      name: 'Cambridge Orchard Park',
      location: 'Cambridge',
      address:"Chieftain Way, Orchard Park,Kings Hedges Road, CB4 2WR, United Kingdom ",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.9,
      price:26.99,
      description: "Ideally located for business meetings near the Cambridge Science and Business Parks.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0616_Cambridge_Orchard_Park_EXT.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },
    {
      id: 16,
      name: 'Cambridge Central',
      location: 'Cambridge',
      address:"Cambridge Leisure Park, Clifton Way,Cambridge, CB1 7DY, United Kingdom",
      from_date:'2020-06-09',
      to_date:'2021-06-01',
      reviews: 4.8,
      price:25.99,
      description: "A short stroll from the Cambridge University Botanic Gardens.",
      image:"https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0615_CAMBRIDGE_CENTRAL_EXTERIOR_2208.jpg",
      rooms:[
        {id:001,name:'Normal Room',facilities:['Free WIFI'],image:"https://pix10.agoda.net/hotelImages/400/400375/400375_15081715350034430963.jpg?s=1024x768"},
        {id:002,name:'Luxury Room',facilities:['Free WIFI','Fridge'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"},
        {id:003,name:'Queen Delux',facilities:['Free WIFI','Fridge','Microwave'],image:"https://www.berjayahotel.com/sites/default/files/hotel-category-offers/kualalumpur/offers-room-berjaya-times-square-hotel-kuala-lumpur.jpg"}
      ]
    },

]

module.exports = { 'hotels': hotels}