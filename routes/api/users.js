const express = require("express");
const router = express.Router();
const data = require('../../data')
//Load controllers
const users=require("../../controllers/user.controller.js")
const hotels =require("../../controllers/hotels.controller")

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", users.register);

  // @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login",users.login);


// @route GET api/users/hotels
// @desc Retrieve all Students
// @access Public
 router.get("/hotels", hotels.findAll);

 router.post("/test",hotels.findOne)


module.exports = router;